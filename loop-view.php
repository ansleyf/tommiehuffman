<article class="uk-article">
	<h2 class="uk-article-title uk-margin-bottom-remove">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</h2>
	<div class="uk-article-meta">
		<i class="uk-icon uk-icon-tags"> <?php the_category(", "); ?></i>
	</div>
	<hr class="uk-article-divider" />
	<?php the_content(); ?>
	<div class="uk-article-meta">
		<i class="uk-icon uk-icon-clock-o"> On <?php the_date(); ?></i><br />
		<i class="uk-icon uk-icon-edit"> By <?php the_author(); ?></i>
		<div class="uk-text-right">
			<a class="uk-button read-more" href="<?php the_permalink(); ?>">Read More</a>
		</div>
	</div>
	<hr class="uk-article-divider" />
</article>